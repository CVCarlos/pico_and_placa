# Pico_and_Placa

Java based application created to determine if a vehicle with defined plate number can be on the streets during a given time on a given date

The program is coded on Java JDK 15
Uses JUnit5 for unit testing and Serenity for cucumber/integration testing

The input is obtained by command line input and uses regex matching to sanitize the inputs 

It was tested on Windows 10 and Linux Mind 20
