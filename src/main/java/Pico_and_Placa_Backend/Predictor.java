package Pico_and_Placa_Backend;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.System.exit;

public class Predictor {

    private static final Calendar calendar = Calendar.getInstance();

    private static final ArrayList<Character> license_plate_exceptions_second_letter =
            new ArrayList<>(Arrays.asList('A','U','Z','W','E'));

    private static final DateFormat time_parser = new SimpleDateFormat("HH:mm");

    //Setting up the time slots limits of 'Pico y Placa'
    private static final String time_slot_start_morning = "06:59";
    private static Date time_slot_start_morning_parsed;
    private static final String time_slot_end_morning = "09:31";
    private static Date time_slot_end_morning_parsed;
    private static final String time_slot_start_afternoon = "15:59";
    private static Date time_slot_start_afternoon_parsed;
    private static final String time_slot_end_afternoon = "19:31";
    private static Date time_slot_end_afternoon_parsed;

    //Setup possible results:
    private static final String safe_to_transit_all_day = "You are safe to transit the " +
            "streets during all the day";
    private static final String safe_to_transit_outside_time_slots = "You are safe to transit" +
            " the streets BUT only outside the 'Pico y Placa' time slots:\n" +
            "  07:00 - 9:30 and 16:00 0 19:30";
    private static final String not_safe_to_transit_inside_time_slots = "You are not safe to " +
            "transit the streets, your time is inside 'Pico y Placa' time slots:\n" +
            "07:00 - 9:30 and 16:00 0 19:30";


    public static void main (String[] args)
    {
        Scanner scanner_in = new Scanner(System.in);
        boolean continue_working;

        boolean date_is_valid = false;

        parseTimeSlotsDates();

        String license_plate;
        String date = null;
        String time;

        System.out.println("===================================================\n");
        System.out.println("""
                Welcome this program will let you know if\s
                 you are able to transit the streets at given time\s
                and date giving the license plate of your car\s
                """);
        System.out.println("===================================================\n");
        do
        {
            System.out.println("Please insert the license plate of your vehicle\n" +
                    "with the format ABC-1234");
            while (!scanner_in.hasNext("\\s*[a-zA-Z][a-zA-Z][a-zA-Z]-\\d\\d\\d\\d\\s*"))
            {
                System.out.println("That plate number does not seems to be correct,\n" +
                        "remember the correct format is ABC-1234");
                scanner_in.nextLine();
            }
            license_plate = scanner_in.next().strip().toUpperCase();
            scanner_in.useDelimiter("\n");
            System.out.println("Now, please insert the date when you are expecting to \n" +
                    "transit on the streets in the format dd MMM yyyy, example 12 Jun 2021");
            while (!date_is_valid)
            {
                scanner_in.nextLine();
                while (!scanner_in.hasNext("\\s*(0?[1-9]|[12][0-9]|3[01])\\s[a-zA-Z][a-zA-Z][a-zA-Z]" +
                        "\\s+\\d\\d\\d\\d\\s*"))
                {
                    System.out.println("That date does not seems to be correct,\n" +
                            "remember the correct format is dd MMM yyyy, example 12 Jun 2021");
                    scanner_in.nextLine();
                }
                date = scanner_in.next().strip().toUpperCase();
                date_is_valid = verifyIfDateExists(date);
                if (!date_is_valid)
                    System.out.println("The date is invalid, please verify the date exist");
            }
            date_is_valid = false;
            System.out.println("Finally, please insert do you expect to transit the streets\n" +
                    "with the 24h format HH:mm, example 13:23 (01:23pm)");
            scanner_in.nextLine();
            while (!scanner_in.hasNext("\\s*([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]\\s*"))
            {
                System.out.println("That time does not seems to be correct,\n" +
                        "remember the correct 24h format is HH:mm, example 13:23 (01:23pm)");
                scanner_in.nextLine();
            }
            time = scanner_in.next().strip();
            scanner_in.nextLine();
            UserRequest user_request = new UserRequest(license_plate,date,time);
            System.out.println("Your inputs are:\n" +
                    "License plate: " + license_plate + "\n" +
                    "Date: " + date + "\n" +
                    "Time: " + time);
            System.out.println("===================================================\n");
            System.out.println(verifyTransitIsAllowed(user_request));
            System.out.println("===================================================\n");
            System.out.println("Do you want to submit a new request? Type Yes(Y)/No(N)");
            while (!scanner_in.hasNext("\\s*([y|Y][e|E][s|S]|[n|N][o|O])\\s*")
                   && !scanner_in.hasNext("\\s*([y|Y]|[n|N]\\s*)"))
            {
                System.out.println("Please, answer Yes(Y) if you want to make another,\n" +
                        "request or No(N) if you do not want to");
                scanner_in.nextLine();
            }
            continue_working = scanner_in.hasNext("\\s*([y|Y][e|E][s|S]|([y|Y]))\\s*");
            scanner_in.nextLine();
        }
        while (continue_working);
        System.out.println("Thanks for using this 'Pico y Placa' predictor service, good bye!");
    }

    public static boolean verifyIfDateExists(String date_to_check)
    {
        DateFormat date_parser = new SimpleDateFormat("dd MMM yyyy");
        date_parser.setLenient(false);
        try {
            date_parser.parse(date_to_check);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /*
    This method provides the prediction based on the license plate, date and time
    - You are safe to transit the streets during all the day
    - You are safe to transit the streets BUT only outside the 'Pico y Placa' time slots:
      07:00 - 9:30 and 16:00 0 19:30
    - You are not safe to transit the streets, your time is inside 'Pico y Placa' time slots:
       07:00 - 9:30 and 16:00 0 19:30
     */
    public static String verifyTransitIsAllowed(UserRequest request)
    {
        if (verifyLicensePlateIsAllowed(request.getLicense_plate(),request.getDate_parsed()))
            return safe_to_transit_all_day;
        else if (!verifyTimeIsInsideTimeSlotBoundaries(request.getTime_parsed()))
            return safe_to_transit_outside_time_slots;
        else
            return not_safe_to_transit_inside_time_slots;
    }

    /*
    This method expects the license_plate on the format ^[a-zA-Z][a-zA-Z][a-zA-Z]\\d\\d\\d\\d?"
    and itinerary_date on format dd MMM yyyy
    The 'Pico y Placa' format that this method is based on is:
    - MONDAY: 1 y 2
    - TUESDAY: 3 y 4
    - WEDNESDAY: 5 y 6
    - THURSDAY: 7 y 8
    - FRIDAY: 9 y 0
    *Exceptions
    Government vehicles - Second letter on license plate is E
    Public transport vehicles - Second letter on license plate is A,U or Z
    Police vehicles - Second letter on license plate is W
    Diplomatic services vehicles - First two letters are CC,CD,OI,AT
    */
    public static boolean verifyLicensePlateIsAllowed(String license_plate, Date itinerary_date)
    {
       // Check for exceptions, date is not important.
       Character second_letter = license_plate.charAt(1);
       if (license_plate_exceptions_second_letter.contains(second_letter)
           || license_plate.matches("(CC|CD|OI|AT).*"))
            return true;

       calendar.setTime(itinerary_date);
       int day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
        return switch (day_of_week) {
            case Calendar.MONDAY -> license_plate.endsWith("1") || license_plate.endsWith("2");
            case Calendar.TUESDAY -> license_plate.endsWith("3") || license_plate.endsWith("4");
            case Calendar.WEDNESDAY -> license_plate.endsWith("5") || license_plate.endsWith("6");
            case Calendar.THURSDAY -> license_plate.endsWith("7") || license_plate.endsWith("8");
            case Calendar.FRIDAY -> license_plate.endsWith("9") || license_plate.endsWith("0");
            default -> true;
        };
    }

    /*
    Verify that a given time is on the time slots boundaries
    Morning: 7:00:59 to 9:30:59
    Afternoon 16:00:59 to 19:30:59
    This method verification are range inclusive
     */
    public static boolean verifyTimeIsInsideTimeSlotBoundaries(Date time_to_verify)
    {
        return ((time_slot_start_morning_parsed.before(time_to_verify) &&
                time_slot_end_morning_parsed.after(time_to_verify)) ||
                (time_slot_start_afternoon_parsed.before(time_to_verify) &&
                time_slot_end_afternoon_parsed.after(time_to_verify)));
    }

    public static void parseTimeSlotsDates()
    {
        try {
            time_slot_start_morning_parsed = time_parser.parse(time_slot_start_morning);
        } catch (ParseException e) {
            System.out.println("Failed to parse time slot start morning " +
                    time_slot_start_morning + ", exiting");
            exit(1);
        }
        try {
            time_slot_end_morning_parsed = time_parser.parse(time_slot_end_morning);
        } catch (ParseException e) {
            System.out.println("Failed to parse time slot end morning " +
                    time_slot_end_morning + ", exiting");
            exit(1);
        }
        try {
            time_slot_start_afternoon_parsed = time_parser.parse(time_slot_start_afternoon);
        } catch (ParseException e) {
            System.out.println("Failed to parse time slot start afternoon " +
                    time_slot_start_afternoon + ", exiting");
            exit(1);
        }
        try {
            time_slot_end_afternoon_parsed = time_parser.parse(time_slot_end_afternoon);
        } catch (ParseException e) {
            System.out.println("Failed to parse time slot end afternoon " +
                    time_slot_end_afternoon + ", exiting");
            exit(1);
        }
    }

}
