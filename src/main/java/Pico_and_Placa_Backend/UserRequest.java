package Pico_and_Placa_Backend;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserRequest
{
    String license_plate;
    Date date_parsed;
    Date time_parsed;

    //Transform the date and time from strings to Java Date format
    DateFormat date_parser = new SimpleDateFormat("dd MMM yyyy");
    DateFormat time_parser = new SimpleDateFormat("HH:mm");

    public UserRequest(String plate, String date_to_parse, String time_to_parse)
    {
        license_plate = plate;
        try
        {
            date_parsed = date_parser.parse(date_to_parse);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse the date" + date_to_parse);
        }
        try
        {
            time_parsed = time_parser.parse(time_to_parse);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse the time" + time_to_parse);
        }
    }

    public UserRequest()
    {
    }


    public void setLicense_plate(String license_plate)
    {
        this.license_plate = license_plate;
    }

    public void setDate_parsed(Date date_parsed)
    {
        this.date_parsed = date_parsed;
    }

    public void setTime_parsed(Date time_parsed)
    {
        this.time_parsed = time_parsed;
    }

    public String getLicense_plate()
    {
        return license_plate;
    }

    public Date getDate_parsed()
    {
        return date_parsed;
    }

    public Date getTime_parsed()
    {
        return time_parsed;
    }
}
