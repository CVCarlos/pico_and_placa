package CucumberTests;

import Pico_and_Placa_Backend.Predictor;
import Pico_and_Placa_Backend.UserRequest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.System.exit;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPredictorSteps {

    private static final String safe_to_transit_all_day = "You are safe to transit the " +
            "streets during all the day";
    private static final String safe_to_transit_outside_time_slots = "You are safe to transit" +
            " the streets BUT only outside the 'Pico y Placa' time slots:\n" +
            "  07:00 - 9:30 and 16:00 0 19:30";
    private static final String not_safe_to_transit_inside_time_slots = "You are not safe to " +
            "transit the streets, your time is inside 'Pico y Placa' time slots:\n" +
            "07:00 - 9:30 and 16:00 0 19:30";

    UserRequest request = new UserRequest();
    private static final DateFormat date_parser = new SimpleDateFormat("dd MMM yyyy");
    private static final DateFormat time_parser = new SimpleDateFormat("HH:mm");

    private static final String valid_license_plate = "ABC-1231";
    private static final String invalid_license_plate = "ABC-1234";
    private static final String testing_date = "14 Jun 2021";
    private static Date testing_date_parsed;

    private static final String time_in_boundaries = "09:00";
    private static Date time_in_boundaries_parsed;
    private static final String time_outside_boundaries = "11:30";
    private static Date time_outside_boundaries_parsed;

    @Given("Given the license plate ^[a-zA-Z][a-zA-Z][a-zA-Z]\\-\\d\\d\\d\\d" +
            " is valid for date dd MMM yyyy")
    public void theLicenseIsValid()
    {
        request.setLicense_plate(valid_license_plate);
        try
        {
            testing_date_parsed = date_parser.parse(testing_date);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse testing date " +
                    testing_date + ", aborting test");
            exit(1);
        }
        request.setDate_parsed(testing_date_parsed);
    }

    @And("And the time HH:mm:ss is inside " + "'Pico y Placa' time slots")
    public void theTimeIsInsideTimeSlot()
    {
       try
       {
           time_in_boundaries_parsed = time_parser.parse(time_in_boundaries);
       } catch (ParseException e)
       {
           System.out.println("Failed to parse time inside boundaries " +
                   time_in_boundaries + ", aborting test");
           exit(1);
       }
       request.setTime_parsed(time_in_boundaries_parsed);
    }

    @Then("Then the itinerary is valid inside 'Pico y Placa' time slots")
    public void theItineraryIsValidAllDay()
    {
        Predictor.parseTimeSlotsDates();
        assertEquals(Predictor.verifyTransitIsAllowed(request),safe_to_transit_all_day);
    }

    @Given("Given the license plate ^[a-zA-Z][a-zA-Z][a-zA-Z]\\-\\d\\d\\d\\d" +
            " is invalid for date dd MMM yyyy")
    public void theLicenseIsInvalid()
    {
        request.setLicense_plate(invalid_license_plate);
        try
        {
            testing_date_parsed = date_parser.parse(testing_date);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse testing date " +
                    testing_date + ", aborting test");
            exit(1);
        }
        request.setDate_parsed(testing_date_parsed);
    }

    @And("And the time HH:mm is outside 'Pico y Placa' " +
            "time slots")
    public void theTimesAreOutsideTimeSlots()
    {
        try
        {
            time_outside_boundaries_parsed = time_parser.parse(time_outside_boundaries);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time outside boundaries " +
                    time_outside_boundaries  + ", aborting test");
            exit(1);
        }
        request.setTime_parsed(time_outside_boundaries_parsed);
    }

    @Then("Then the itinerary is valid outside 'Pico y Placa' time slots")
    public void theItineraryIsValidOutsideTimeSlots()
    {
        Predictor.parseTimeSlotsDates();
        assertEquals(Predictor.verifyTransitIsAllowed(request),safe_to_transit_outside_time_slots);
    }

    /*
    This comment is here to improve test readability
    @Given("Given the license plate ^[a-zA-Z][a-zA-Z][a-zA-Z]\\-\\d\\d\\d\\d" +
         " is invalid for date dd MMM yyyy")
    public void theLicenseIsInvalid()
    {
    }

    @And("And the time HH:mm:ss is inside 'Pico y Placa' +
             time slots")
    public void theTimesAreInsideTimeSlotsSameDay()
    {
    }
    */
    @Then("Then the itinerary is invalid inside 'Pico y Placa' time slots")
    public void theItineraryIsInvalidInsideSlots()
    {
        Predictor.parseTimeSlotsDates();
        assertEquals(Predictor.verifyTransitIsAllowed(request),not_safe_to_transit_inside_time_slots);
    }
}