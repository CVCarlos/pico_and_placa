package CucumberTests;

import net.serenitybdd.junit5.SerenityTest;
import net.thucydides.core.annotations.Steps;
import org.junit.jupiter.api.Test;


@SerenityTest
public class TestPredictorStories
{

    @Steps
    private TestPredictorSteps predictorSteps;

    /*
    License plate is ok to transit that day
    */
    @Test
    void plateIsValidAndTimeIsValidAllDay()
    {
        predictorSteps.theLicenseIsValid();
        predictorSteps.theTimeIsInsideTimeSlot();
        predictorSteps.theItineraryIsValidAllDay();
    }

    /*
    License plate is not ok to transit that day but
    time is outside "Pico y Placa" time slots
    */
    @Test
    void plateIsInvalidButTimeIsValidOutsideTimeSlots()
    {
        predictorSteps.theLicenseIsInvalid();
        predictorSteps.theTimesAreOutsideTimeSlots();
        predictorSteps.theItineraryIsValidOutsideTimeSlots();
    }

    /*
    License plate is not ok to transit that day and start
    and end time is inside "Pico y Placa" time slots
    */
    @Test
    void plateIsInvalidAndTimeIsInvalid()
    {
        predictorSteps.theLicenseIsInvalid();
        predictorSteps.theTimeIsInsideTimeSlot();
        predictorSteps.theItineraryIsInvalidInsideSlots();
    }
}
