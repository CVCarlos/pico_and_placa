package JUnit5Tests;

import Pico_and_Placa_Backend.Predictor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

import static java.lang.System.exit;

public class TestPredictorTimeVerifier {

    private static DateFormat time_parser = new SimpleDateFormat("HH:mm");

    private static final String time_in_morning_boundaries1 = "07:00";
    private static Date time_in_morning_boundaries1_parsed;
    private static final String time_in_morning_boundaries2 = "08:10";
    private static Date time_in_morning_boundaries2_parsed;
    private static final String time_in_morning_boundaries3 = "09:30";
    private static Date time_in_morning_boundaries3_parsed;
    private static final String time_in_afternoon_boundaries1 = "16:00";
    private static Date time_in_afternoon_boundaries1_parsed;
    private static final String time_in_afternoon_boundaries2 = "18:10";
    private static Date time_in_afternoon_boundaries2_parsed;
    private static final String time_in_afternoon_boundaries3 = "19:30";
    private static Date time_in_afternoon_boundaries3_parsed;
    private static final String time_outside_boundaries1 = "06:30";
    private static Date time_outside_boundaries1_parsed;
    private static final String time_outside_boundaries2 = "12:22";
    private static Date time_outside_boundaries2_parsed;
    private static final String time_outside_boundaries3 = "21:30";
    private static Date time_outside_boundaries3_parsed;

    @BeforeAll
    private static void parseTimes()
    {
        try
        {
                time_in_morning_boundaries1_parsed = time_parser.parse(time_in_morning_boundaries1);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time slot on morning 1 " +
                        time_in_morning_boundaries1 + ", exiting");
            exit(1);
        }

        try
        {
            time_in_morning_boundaries2_parsed = time_parser.parse(time_in_morning_boundaries2);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time slot on morning 2 " +
                    time_in_morning_boundaries2 + ", exiting");
            exit(1);
        }

        try
        {
            time_in_morning_boundaries3_parsed = time_parser.parse(time_in_morning_boundaries3);
        } catch (ParseException e) {
            System.out.println("Failed to parse time slot on morning 3 " +
                    time_in_morning_boundaries3 + ", exiting");
            exit(1);
        }

        try
        {
            time_in_afternoon_boundaries1_parsed = time_parser.parse(time_in_afternoon_boundaries1);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time slot on afternoon 1 " +
                    time_in_afternoon_boundaries1 + ", exiting");
            exit(1);
        }

        try
        {
            time_in_afternoon_boundaries2_parsed = time_parser.parse(time_in_afternoon_boundaries2);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time slot on afternoon 2 " +
                    time_in_afternoon_boundaries2 + ", exiting");
            exit(1);
        }

        try
        {
            time_in_afternoon_boundaries3_parsed = time_parser.parse(time_in_afternoon_boundaries3);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time slot on afternoon 3 " +
                    time_in_afternoon_boundaries3 + ", exiting");
            exit(1);
        }

        try
        {
            time_outside_boundaries1_parsed = time_parser.parse(time_outside_boundaries1);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time slot outside boundaries 1 " +
                    time_outside_boundaries1 + ", exiting");
            exit(1);
        }

        try
        {
            time_outside_boundaries2_parsed = time_parser.parse(time_outside_boundaries2);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time slot outside boundaries 2 " +
                    time_outside_boundaries2 + ", exiting");
            exit(1);
        }

        try
        {
            time_outside_boundaries3_parsed = time_parser.parse(time_outside_boundaries3);
        } catch (ParseException e)
        {
            System.out.println("Failed to parse time outside boundaries 3 " +
                    time_outside_boundaries3 + ", exiting");
            exit(1);
        }
    }

    @Test
    void testVerifyTimeInsideSlotMorningBoundaries()
    {
        Predictor.parseTimeSlotsDates();
        assertTrue(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_in_morning_boundaries1_parsed));
        assertTrue(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_in_morning_boundaries2_parsed));
        assertTrue(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_in_morning_boundaries3_parsed));
    }

    @Test
    void testVerifyTimeInsideSlotAfternoonBoundaries()
    {
        Predictor.parseTimeSlotsDates();
        assertTrue(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_in_afternoon_boundaries1_parsed));
        assertTrue(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_in_afternoon_boundaries2_parsed));
        assertTrue(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_in_afternoon_boundaries3_parsed));
    }

    @Test
    void testVerifyTimeOutsideSlotsBoundaries()
    {
        Predictor.parseTimeSlotsDates();
        assertFalse(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_outside_boundaries1_parsed));
        assertFalse(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_outside_boundaries2_parsed));
        assertFalse(Predictor.verifyTimeIsInsideTimeSlotBoundaries(time_outside_boundaries3_parsed));
    }
}
