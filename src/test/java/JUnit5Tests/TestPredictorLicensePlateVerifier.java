package JUnit5Tests;

import Pico_and_Placa_Backend.Predictor;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.System.exit;
import static org.junit.jupiter.api.Assertions.*;

public class TestPredictorLicensePlateVerifier {

    DateFormat date_parser = new SimpleDateFormat("dd MMM yyyy");

    // Set up the licences plates with last digit from 0 to 9 (one per each number
    private static final String license_plate_1 = "ABC-4321";
    private static final String license_plate_2 = "ABC-4312";
    private static final String license_plate_3 = "ABC-4123";
    private static final String license_plate_4 = "ABC-3214";
    private static final String license_plate_5 = "ABC-2345";
    private static final String license_plate_6 = "ABC-3456";
    private static final String license_plate_7 = "ABC-4567";
    private static final String license_plate_8 = "ABC-5678";
    private static final String license_plate_9 = "ABC-6789";
    private static final String license_plate_0 = "ABC-7890";

    // Set up the dates, one per each day of the week (Monday - Friday
    private static final String monday_date_string = "14 June 2021";
    private static Date monday_date_parsed;
    {
        try {
            monday_date_parsed = date_parser.parse(monday_date_string);
        } catch (Exception e) {
            System.out.println("Failed to parse morning date " +
                    monday_date_string + ", test aborted");
            exit(1);
        }
    }

    private static final String tuesday_date_string = "15 June 2021";
    private static Date tuesday_date_parsed;
    {
        try {
            tuesday_date_parsed = date_parser.parse(tuesday_date_string);
        } catch (Exception e) {
            System.out.println("Failed to parse tuesday date " +
                    tuesday_date_string + ", test aborted");
            exit(1);
        }
    }

    private static final String wednesday_date_string = "16 June 2021";
    private static Date wednesday_date_parsed;
    {
        try {
            wednesday_date_parsed = date_parser.parse(wednesday_date_string);
        } catch (Exception e) {
            System.out.println("Failed to parse wednesday date " +
                    wednesday_date_string + ", test aborted");
            exit(1);
        }
    }

    private static final String thursday_date_string = "17 June 2021";
    private static Date thursday_date_parsed;
    {
        try {
            thursday_date_parsed = date_parser.parse(thursday_date_string);
        } catch (Exception e) {
            System.out.println("Failed to parse thursday date " +
                    thursday_date_string + ", test aborted");
            exit(1);
        }
    }

    private static final String friday_date_string = "18 June 2021";
    private static Date friday_date_parsed;
    {
        try {
            friday_date_parsed = date_parser.parse(friday_date_string);
        } catch (Exception e) {
            System.out.println("Failed to parse friday date " +
                    friday_date_string + ", test aborted");
            exit(1);
        }
    }

    /*
    Date should not matter for this test but to catch any error the date is
    monday at last digit is 3. A normal vehicle with this license plate should
    fail but not one within the exceptions.
    Government vehicles - Second letter on license plate is E
    */
    @Test
    void testVerifyGovernmentVehicleIsExempted()
    {
        String license_plate_E3 = "AEC4323";
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_E3,monday_date_parsed));
    }

    /*
    Date should not matter for this test but to catch any error the date is
    monday at last digit is 3. A normal vehicle with this license plate should
    fail but not one within the exceptions.
    Public transport vehicles - Second letter on license plate is A,U or Z
    */
    @Test
    void testVerifyPublicTransportVehicleIsExempted()
    {
        String license_plate_A3 = "AAC4323";
        String license_plate_U3 = "AUC4323";
        String license_plate_Z3 = "AZC4323";
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_A3,monday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_U3,monday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_Z3,monday_date_parsed));
    }

    /*
    Date should not matter for this test but to catch any error the date is
    monday at last digit is 3. A normal vehicle with this license plate should
    fail but not one within the exceptions.
    Police vehicles - Second letter on license plate is W
    */
    @Test
    void testVerifyPoliceVehicleIsExempted()
    {
        String license_plate_W3 = "AWC4323";
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_W3,monday_date_parsed));
    }

    /*
    Date should not matter for this test but to catch any error the date is
    monday at last digit is 3. A normal vehicle with this license plate should
    fail but not one within the exceptions.
    Diplomatic services vehicles - First two letters are CC,CD,OI,AT
    */
    @Test
    void testVerifyDiplomaticServiceVehicleIsExempted()
    {
        String license_plate_CC3 = "CCE4323";
        String license_plate_CD3 = "CDF4323";
        String license_plate_OI3 = "OIZ4323";
        String license_plate_AT3 = "ATO4323";

        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_CC3,monday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_CD3,monday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_OI3,monday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_AT3,monday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsAllowedMonday()
    {
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_1,monday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_2,monday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsAllowedTuesday()
    {
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_3,tuesday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_4,tuesday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsAllowedWednesday()
    {
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_5,wednesday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_6,wednesday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsAllowedThursday()
    {
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_7,thursday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_8,thursday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsAllowedFriday()
    {
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_9,friday_date_parsed));
        assertTrue(Predictor.verifyLicensePlateIsAllowed(license_plate_0,friday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsNotAllowedMonday()
    {
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_3,monday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_4,monday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_5,monday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_6,monday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_7,monday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_8,monday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_9,monday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_0,monday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsNotAllowedTuesday()
    {
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_1,tuesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_2,tuesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_5,tuesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_6,tuesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_7,tuesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_8,tuesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_9,tuesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_0,tuesday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsNotAllowedWednesday()
    {
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_1,wednesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_2,wednesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_3,wednesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_4,wednesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_7,wednesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_8,wednesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_9,wednesday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_0,wednesday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsNotAllowedThursday()
    {
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_1,thursday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_2,thursday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_3,thursday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_4,thursday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_5,thursday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_6,thursday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_9,thursday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_0,thursday_date_parsed));
    }

    @Test
    void testVerifyLicensePlateIsNotAllowedFriday()
    {
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_1,friday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_2,friday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_3,friday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_4,friday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_5,friday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_6,friday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_7,friday_date_parsed));
        assertFalse(Predictor.verifyLicensePlateIsAllowed(license_plate_8,friday_date_parsed));
    }
}
